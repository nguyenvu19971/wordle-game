import React from "react";
import { memo } from "react";
import helpSVG from "../../../assets/icons-svg/help.svg";
import statisticSVG from "../../../assets/icons-svg/statistics.svg";
import playSVG from "../../../assets/icons-svg/play.svg";
import settingSVG from "../../../assets/icons-svg/setting.svg";
import "./index.scss";
import SmallButton from "../../buttons/btn-small/SmallButton";

const Header = () => {
  return (
    <div className="header">
      <div className="btn-box">
        <SmallButton icon={helpSVG} className="has-margin-right" />
        <SmallButton icon={statisticSVG} />
      </div>
      <div className="texts">
        <div className="text text-title">WORDLE</div>
        <div className="text text-sub-title">DAILY MODE</div>
      </div>
      <div className="btn-box">
        <SmallButton icon={playSVG} className="has-margin-right" />
        <SmallButton icon={settingSVG} />
      </div>
    </div>
  );
};

export default memo(Header);
