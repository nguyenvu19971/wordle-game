import React from "react";
import { memo } from "react";
import Modal from "../../../components/modal";

import './index.scss'

const ModalStatistics = (props) => {
  return (
    <Modal className="modal-statistics" header={{ title: "STATISTICS" }}>
      <div className="statistics">
        <div className="s-item">
          <div className="text text-36 value">3</div>
          <div className="text text-14">Played</div>
        </div>

        <div className="s-item">
          <div className="text text-36 value">33</div>
          <div className="text text-14">Win %</div>
        </div>

        <div className="s-item">
          <div className="text text-36 value">1</div>
          <div className="text text-14">Current Streak</div>
        </div>

        <div className="s-item">
          <div className="text text-36 value">1</div>
          <div className="text text-14">Max Streak</div>
        </div>
      </div>

      <div className="guess-distribution">
        <div className="text text-16 title">Guess Distribution</div>
        <div className="graph-container">
          <div className="graph-item"> 
            <div className="">1</div>
            <div className="graph-bar">0</div>
          </div>
          <div className="graph-item"> 
            <div className="">2</div>
            <div className="graph-bar">0</div>
          </div>
          <div className="graph-item"> 
            <div className="">3</div>
            <div className="graph-bar">0</div>
          </div>
          <div className="graph-item"> 
            <div className="">4</div>
            <div className="graph-bar">0</div>
          </div>
          <div className="graph-item"> 
            <div className="">5</div>
            <div className="graph-bar">0</div>
          </div>
          <div className="graph-item"> 
            <div className="">6</div>
            <div className="graph-bar">0</div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default memo(ModalStatistics);
