import React from "react";
import { memo } from "react";
import SmallButton from "../../buttons/btn-small/SmallButton";

import iconDelete from "../../../assets/icons-svg/delete.svg";

const flagCheck = require("../../../assets/icons-img/check.png");

const keyBoardText = [
  ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
  ["A", "S", "D", "F", "G", "H", "J", "K", "L"],
  ["CHECK", "Z", "X", "C", "V", "B", "N", "M", "ENTER"],
];

const Keyboard = (props) => {
  const handleClick = (char) => () => {
    console.log('WWWWWWWW ', char, props)

    if (props.onHandleInput) {
      console.log(123)
      props.onHandleInput(char);
    }
  };

  const { keysInput } = props

  return (
    <div className="key-board-container">
      {keyBoardText.map((item, idx) => {
        return (
          <div key={`key-rows-${idx}`} className="key-board-rows">
            {item.map((k) => {
              let text = k;
              let icon = null;
              let className = "";

              switch (k) {
                case "CHECK":
                  text = null;
                  icon = flagCheck;
                  className = "flex";
                  break;

                case "ENTER":
                  text = null;
                  icon = iconDelete;
                  className = "flex";
                  break;

                default:
                  text = k;
                  icon = null;
                  break;
              }

              return (
                <div className={`key-item ${className}`} key={`key-board-${k}`}>
                  <SmallButton className={keysInput[k]} onClick={handleClick(k)} title={text} icon={icon} />
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default memo(Keyboard);
