import React from "react";
import { memo } from "react";
import Button from "../../../components/buttons";
import './index.scss'

const SimpleButton = (props) => {
  return (
    <Button className={`btn-simple ${props.className}`} onClick={props.onClick}>
      <div className="">
        {props.title && <div className="text title">{props.title}</div>}
        {props.icon && <img className="icon" src={props.icon} alt=""/>}
      </div>
    </Button>
  );
};

export default memo(SimpleButton);
