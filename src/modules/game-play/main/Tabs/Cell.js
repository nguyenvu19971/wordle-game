import React from "react";
import { memo } from "react";

const Cell = (props) => {
  return <div className={`cell-box ${props.className}`}>{props.children}</div>;
};

export default memo(Cell);
