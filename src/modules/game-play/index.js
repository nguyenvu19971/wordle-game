import React, { useEffect, useState } from "react";
import { memo } from "react";
import Footer from "./footer";
import Header from "./header/index";
import Main from "./main/index";

import "./index.scss";

const tabs = [
  { key: "tab-4", value: 4, size: { rows: 6, cols: 4 } },
  { key: "tab-5", value: 5, size: { rows: 6, cols: 5 } },
  { key: "tab-6", value: 6, size: { rows: 6, cols: 6 } },
  { key: "tab-7", value: 7, size: { rows: 6, cols: 7 } },
  { key: "tab-8", value: 8, size: { rows: 6, cols: 8 } },
];

const defaultTab = 5;

const GamePlay = () => {
  const [keyword, setKeyWord] = useState("");
  const [listInput, setListInput] = useState([]);
  const [currentInput, setCurrentInput] = useState("");
  const [col, setCol] = useState(0);

  const [tabIdx, setTabIdx] = useState(defaultTab);
  const [boardSize, setBoardSize] = useState(tabs[1].size);
  const [stateCellInput, setStateCellInput] = useState([]);
  const [keysInput, setKeysInput] = useState({});

  const onHandleInput = (char) => {
    switch (char) {
      case "ENTER":
        handleClearChar();
        return;

      case "CHECK":
        checkCurrentInput();
        return;

      default:
        break;
    }

    if (currentInput.length >= boardSize.cols) {
      return;
    }

    const text = currentInput + char;

    setCurrentInput(text);

    insertListInput(text);
  };

  const handleClearChar = () => {
    if (!currentInput) return;

    let text = currentInput;

    text = text.slice(0, -1);

    console.log("text === ", text);

    setCurrentInput(text);

    deleteListInput(text);
  };

  const checkCurrentInput = () => {
    if (currentInput.length < boardSize.cols) return;

    console.log("Current input", currentInput);

    const answer = "PENTA"

    const charInput = currentInput.split('')
    let sListInput = [...stateCellInput]
    let state = ""

    for (let i = 0; i < charInput.length; i++) {
      if(answer.includes(charInput[i])){
        if(charInput[i] === answer.charAt(i)){
          state += '1'
          setKeysInput(keysInput => ({...keysInput, [charInput[i]]: 'key-correct-spot'}))
          continue
        }
        state += '2'
        if(keysInput[charInput[i]] !== 'key-correct-spot'){
          setKeysInput(keysInput => ({...keysInput, [charInput[i]]: 'key-wrong-spot'}))
        }
        continue
      }
      setKeysInput(keysInput => ({...keysInput, [charInput[i]]: 'key-no-spot'}))
      state += '0'
    }

    sListInput[col] = state
    setStateCellInput([...sListInput])

    setCurrentInput('')
    setCol(col + 1)
  };

  const insertListInput = (text) => {
    const cListInput = [...listInput];

    for (let i = 0; i < boardSize.rows; i++) {
      if (!cListInput[i] || cListInput[i].length < boardSize.cols) {
        cListInput[i] = text;

        break;
      }
    }

    setListInput([...cListInput]);

    console.log("cListInput === ", cListInput);
  };

  const deleteListInput = (text) => {
    const cListInput = [...listInput];

    cListInput[cListInput.length - 1] = text;

    setListInput([...cListInput]);

    console.log("cListInput === ", cListInput);
  };

  const onClickTab = (idx) => () => {
    const { value, size } = tabs[idx];

    setTabIdx(value);
    setBoardSize(size);
  };

  return (
    <div className="game-play">
      <Header />

      <Main
        defaultTab={defaultTab}
        tabIdx={tabIdx}
        tabs={tabs}
        listInput={listInput}
        currentInput={currentInput}
        onClickTab={onClickTab}
        boardSize={boardSize}
        stateCellInput={stateCellInput}
      />

      <Footer onHandleInput={onHandleInput} keysInput={keysInput}/>
    </div>
  );
};

export default memo(GamePlay);
