import React from "react";
import { memo } from "react";
import Cell from "./Cell";

const Board = (props) => {
  const getClassNameByStateCell = (number) => {
    if(number === '1') return 'correct-spot'
    if(number === '2') return 'wrong-spot'
    if(number === '0') return 'no-spot'
    return ""
  }

  const renderBoard = () => {
    const { size = { rows: 0, cols: 0 }, listInput } = props;
    const { rows, cols } = size;
    const { stateCellInput } = props

    return new Array(rows).fill().map((r, iR) => {
      const input = listInput[iR] || ''
      const lInput = input.split('')

      const stateCell = stateCellInput[iR] || ''
      const lStateCell = stateCell.split('')

      return (
        <div className="row-container" key={`cell-row-${iR}`}>
          {new Array(cols).fill().map((c, iC) => {
            return <Cell key={`cell-col-${iC}`} className={`cell-item ${getClassNameByStateCell(lStateCell[iC])}`}>{lInput[iC]}</Cell>;
          })}
        </div>
      );
    });
  };

  return <div className="board-container">{renderBoard()}</div>;
};

export default memo(Board);
