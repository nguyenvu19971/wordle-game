import React from "react";
import { memo } from "react";

import "./index.scss";

const Toggle = (props) => {
  return (
    <div className={`toggle ${props.className || ""}`} onClick={props.onClick}>
      <label className="label">
        <label className="text text-16 title">{props.label}</label>
        <label className="text text-12 description">{props.description}</label>
      </label>

      <div className="checkbox">
        <input type="checkbox" id="toggle" />
        <label className="label-checkbox" htmlFor="toggle"></label>
      </div>
    </div>
  );
};

export default memo(Toggle);
