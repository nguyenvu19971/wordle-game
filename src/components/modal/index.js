import React, { useState } from "react";
import { memo } from "react";
import "./index.scss";
import iconClose from "../../assets/icons-svg/close.svg";

const Modal = (props) => {
  const [show, setShow] = useState(false);

  const handleShowModal = () => {
    setShow(true);
  };

  const handleCloseModal = () => {
    setShow(false);
  };

  return (
    <div>
      <div className="button text" onClick={handleShowModal}>
        Show modal
      </div>

      {show && (
        <div className={`modal ${props.className}`}>
          <div className="content">
            <div className="header">
              <div className="title text text-16">{props.header?.title}</div>
              <div className="icon-close" onClick={handleCloseModal}>
                <img src={iconClose} alt="Close" />
              </div>
            </div>
            <div className="main">{props.children}</div>
            <div className="footer">{props.footer}</div>
          </div>
        </div>
      )}
    </div>
  );
};

export default memo(Modal);
