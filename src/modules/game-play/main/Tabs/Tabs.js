import React from "react";
import { memo } from "react";

const Tabs = (props) => {
  return (
    <div className="tab-box">
      <div className="tab-header">
        <span className="text">Letters:</span>
        <div className="tab-level">
          {props.tabs.map((item, idx) => {
            return (
              <div
                key={item.key}
                className={`tab-level-item ${
                  item.value === props.tabIdx ? "active" : ""
                }`}
                onClick={props.onClickTab(idx)}
              >
                {item.value}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default memo(Tabs);
