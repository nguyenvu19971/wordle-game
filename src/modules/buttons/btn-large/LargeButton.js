import React from "react";
import { memo } from "react";
import Button from "../../../components/buttons";
import "./index.scss";

const LargeButton = (props) => {
  return (
    <Button className={`btn-large ${props.className}`} onClick={props.onClick}>
      <div className="b-content">
        {props.icon && (
          <div className="text text-18 icon">
            <img src={props.icon} alt="" />
          </div>
        )}
        {props.title && <div className="text text-18 title">{props.title}</div>}
        {props.description && (
          <div className="text text-12 description">{props.description}</div>
        )}
      </div>
    </Button>
  );
};

export default memo(LargeButton);
