import React from "react";
import { memo } from "react";
import Modal from "../../../components/modal";
import LargeButton from "../../buttons/btn-large/LargeButton";
import './index.scss'

const ModalMode = (props) => {
  return (
    <Modal className="modal-mode" header={{ title: "PLAY MODE" }}>
      <LargeButton title="Daily mode" description="One word per level per day" />
      <LargeButton title="Unlimited mode" description="Unlimited words" />
      <LargeButton title="Create mode" description="Create your word and share it" />
    </Modal>
  );
};

export default memo(ModalMode);
