import React from "react";
import { memo } from "react";
import Modal from "../../../components/modal";
import Toggle from "../../../components/toggle";
import './index.scss'

const ModalSetting = (props) => {
  return (
    <Modal header={{ title: "SETTINGS" }}>
      <div className="setting-content">
        <div className="s-item s-hard-mode">
          <Toggle
            label="Hard Mode"
            description="Any revealed hints must be used in subsequent guesses"
          />
        </div>

        <div className="s-item s-theme">
          <Toggle label="Dark Theme" />
        </div>

        <div className="s-item s-color-blind-mode">
          <Toggle label="Color Blind Mode" description="High contrast colors" />
        </div>

        <div className="s-item s-language">
          <div className="text title">Language</div>
          <div className="choose-language">
            <div className="icon-flag"><img src="" alt="" /></div>
            <div className="text-nation text text-16">English</div>
          </div>
        </div>

        <div className="s-item s-contact">
        <div className="text title">Contact</div>
          <div className="contact">
            Sun@Studio.io
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default memo(ModalSetting);
