import React from "react";
import { memo } from "react";
import Modal from "../../../components/modal";
import "./index.scss";

const ModalGuide = (props) => {
  return (
    <Modal className="modal-guide">
      <div className="text">Guess the word in 6 tries.</div>
      <div className="text">
        After each guess, the color of the tiles will change to show how close
        your guess was to the word.
      </div>
      <div className="example">
        {["T", "A", "B", "L", "E"].map((item, idx) => {
          return (
            <div className={`example-item ${idx === 0 ? "correct" : ""}`}>
              {item}
            </div>
          );
        })}
      </div>
      <div className="text">
        The letter <span className="">T</span> is in the word and in the
        correct. spot.
      </div>
      <div className="example">
        {["P", "A", "Y", "E", "R"].map((item, idx) => {
          return (
            <div className={`example-item ${idx === 2 ? "wrong" : ""}`}>
              {item}
            </div>
          );
        })}
      </div>
      <div className="text">
        The letter <span className="">Y</span> is in the word but in the wrong
        spot. spot.
      </div>  
      <div className="example">
        {["O", "T", "H", "E", "R"].map((item, idx) => {
          return (
            <div className={`example-item ${idx === 4 ? "not-exist" : ""}`}>
              {item}
            </div>
          );
        })}
      </div>
      <div className="text">
        The letter <span className="">r</span> is not in the word in any spot.
        spot.
      </div>
    </Modal>
  );
};

export default memo(ModalGuide);
