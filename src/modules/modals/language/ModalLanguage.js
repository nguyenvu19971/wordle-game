import React from "react";
import { memo } from "react";
import Modal from "../../../components/modal";
import LargeButton from "../../buttons/btn-large/LargeButton";
import "./index.scss";

const flagEnglish = require("../../../assets/icons-img/flag-english.png");
const flagFrance = require("../../../assets/icons-img/flag-france.png");
const flagGermany = require("../../../assets/icons-img/flag-germany.png");
const flagPortugal = require("../../../assets/icons-img/flag-portugal.png");
const flagRussia = require("../../../assets/icons-img/flag-russia.png");
const flagSpain = require("../../../assets/icons-img/flag-spain.png");
const flagItaly = require("../../../assets/icons-img/flag-italy.png");


const language = [
  { icon: flagRussia, title: "Русский" },
  { icon: flagSpain, title: "Español" },
  { icon: flagPortugal, title: "Português" },
  { icon: flagItaly, title: "Italiano" },
  { icon: flagGermany, title: "Deutsch" },
  { icon: flagFrance, title: "Français" },
  { icon: flagEnglish, title: "English" },
];

const ModalLanguage = (props) => {
  return (
    <Modal className="modal-language" header={{ title: "LANGUAGE" }}>
      {language.map((item) => {
        return <LargeButton key={item.title} className="btn-language" title={item.title} icon={item.icon} />;
      })}
    </Modal>
  );
};

export default memo(ModalLanguage);
