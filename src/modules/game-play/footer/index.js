import React from "react";
import { memo } from "react";
import Keyboard from "./keyboard";

import "./index.scss"

const Footer = (props) => {
  return (
    <div className="footer-container">
        <Keyboard onHandleInput={props.onHandleInput} keysInput={props.keysInput}/>
    </div>
  );
};

export default memo(Footer);
