import React from "react";
import { memo } from "react";
import Tabs from "./Tabs/Tabs";
import "./index.scss";
import Board from "./Tabs/Board";

const Main = (props) => {
  return (
    <div className="main-content">
      <Tabs
        tabs={props.tabs}
        tabIdx={props.tabIdx}
        onClickTab={props.onClickTab}
      />

      <Board
        size={props.boardSize}
        listInput={props.listInput}
        currentInput={props.currentInput}
        stateCellInput={props.stateCellInput}
      />
    </div>
  );
};

export default memo(Main);
