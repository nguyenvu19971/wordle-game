import { useEffect, useState } from "react";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import { MODE_DARK, MODE_LIGHT } from "./constants/mode";
import logo from "./logo.svg";
import GamePlay from "./modules/game-play";
import appRoutes from "./routers/routes";
import "./styles/index.scss";

function Navigator() {
  return (
    <nav>
      <Link className="text" to={"/"}>
        Home
      </Link>{" "}
      <br />
      <Link className="text" to={"/game-play"}>
        GamePlay
      </Link>{" "}
      <br />
      <Link className="text" to={"/components"}>
        Components
      </Link>{" "}
      <br />
    </nav>
  );
}

function App() {
  const [mode, setMode] = useState(MODE_LIGHT);

  const handleSwitchMode = () => {
    switch (mode) {
      case MODE_LIGHT:
        setMode(MODE_DARK);
        break;

      case MODE_DARK:
        setMode(MODE_LIGHT);
        break;

      default:
        setMode(MODE_DARK);
        break;
    }
  };

  useEffect(() => {
    document.documentElement.setAttribute("data-theme", mode);
  }, [mode]);

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {appRoutes.map((route, idx) => (
            <Route
              key={idx}
              path={route.path}
              exact={route.exact}
              element={<route.component />}
            />
          ))}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
