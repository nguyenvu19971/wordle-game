import React from "react";
import { memo } from "react";
import Modal from "../../components/modal";
import ModalSetting from "../modals/setting/ModalSetting";
import Toggle from "../../components/toggle";
import LargeButton from "../buttons/btn-large/LargeButton";
import ModalMode from "../modals/mode/ModalMode";
import ModalStatistics from "../modals/statistics/ModalStatistics";
import ModalGuide from "../modals/guide/ModalGuide";
import ModalLanguage from "../modals/language/ModalLanguage";

const Components = (props) => {
  return (
    <div>
      <h1 className="text">Components</h1>

      <h2 className="text">Text</h2>

      <div className="text">Text demo</div>
      <div className="text text-12">Text 12</div>
      <div className="text text-14">Text 14</div>
      <div className="text text-16">Text 16</div>
      <div className="text text-18">Text 18</div>

      <h2 className="text">Modal</h2>
      <Modal header={{ title: "Demo" }}>Demo</Modal>

      <ModalSetting />

      <ModalMode />

      <ModalStatistics />

      <ModalGuide/>

      <ModalLanguage />

      <h2 className="text">Toggle</h2>
      <Toggle label={"Label demo"} description={"Description demo"} />

      <h2 className="text">Button</h2>

      <div style={{ width: "200px" }}>
        <LargeButton title="Demo" description="Demo" />
      </div>
    </div>
  );
};

export default memo(Components);
