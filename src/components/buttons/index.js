import React from "react";
import { memo } from "react";
import "./index.scss";

const Button = (props) => {
  return (
    <div className={`button ${props.className}`} onClick={props.onClick}>
      {props.children}
    </div>
  );
};

export default memo(Button);
