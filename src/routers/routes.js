import Home from "../modules/home";
import GamePlay from "../modules/game-play";
import Components from "../modules/components";

const appRoutes = [
  { path: "", exact: true, component: Home },
  { path: "game-play", exact: true, component: GamePlay },
  { path: "components", exact: true, component: Components },
];

export default appRoutes;
